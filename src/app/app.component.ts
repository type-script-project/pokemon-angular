import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
    template: `<h1>Welcome to pokemons[0]`
})
export class AppComponent {
  pokemons = ['Bulbizzarre', 'Salmèche', 'Carapuce'];
}
